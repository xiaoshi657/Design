﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 命令模式
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleRemoteControl remote = new SimpleRemoteControl();
            Light light = new Light();
            LightOnCommand lighton = new LightOnCommand(light);
            remote.SetCommand(lighton);
            remote.ButtonWasPressed();
            Console.ReadKey();
        }
    }
}
