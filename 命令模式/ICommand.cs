﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 命令模式
{
    public interface ICommand
    {
        void Execute();
    }
}
