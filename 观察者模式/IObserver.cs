﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 观察者模式
{
    public interface IObserver
    {
        void update(float temperature,float humidity,float pressure);
    }
}
