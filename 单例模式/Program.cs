﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace 单例模式
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread thread1 = new Thread(GetSingletonClass);
            Thread thread2 = new Thread(GetSingletonClass);
            Thread thread3 = new Thread(GetSingletonClass);
            Thread thread4 = new Thread(GetSingletonClass);
            Thread thread5 = new Thread(GetSingletonClass);
            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();
            thread5.Start();

            Console.ReadKey();
        }

        public static void GetSingletonClass()
        {
            Console.WriteLine(SingletonClass.GetInstance().GetHashCode());
        }
    }
}
