﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 代理模式
{
    public interface IMath
    {
        double Add(double num1, double num2);
        double Sub(double num1, double num2);
        double Mul(double num1, double num2);
        double Div(double num1, double num2);
    }
}
