﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 代理模式
{
    class Program
    {
        static void Main(string[] args)
        {
            MathProxy proxy = new MathProxy();

            Console.WriteLine(proxy.Add(2, 6));
            Console.WriteLine(proxy.Sub(88, 2));
            Console.WriteLine(proxy.Mul(22, 55));
            Console.WriteLine(proxy.Div(5, 0));

            Console.ReadKey();
        }
    }
}
