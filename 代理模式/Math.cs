﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 代理模式
{
    public class Math : MarshalByRefObject,IMath
    {
        #region IMath 成员

        public double Add(double num1, double num2)
        {
            return num1 + num2;
        }

        public double Sub(double num1, double num2)
        {
            return num1 - num2;
        }

        public double Mul(double num1, double num2)
        {
            return num1 * num2;
        }

        public double Div(double num1, double num2)
        {
            return num2 == 0 ? 0 : num1 / num2;
        }

        #endregion
    }
}
