﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Remoting;

namespace 代理模式
{
    public class MathProxy:IMath
    {
        Math math = null;

        public MathProxy()
        {
            AppDomain domain = AppDomain.CreateDomain("MathDomain",null,null);
            ObjectHandle obj = domain.CreateInstance("代理模式", "代理模式.Math");
            math = obj.Unwrap() as Math;
        }

        #region IMath 成员

        public double Add(double num1, double num2)
        {
            return math.Add(num1,num2);
        }

        public double Sub(double num1, double num2)
        {
            return math.Sub(num1, num2);
        }

        public double Mul(double num1, double num2)
        {
            return math.Mul(num1, num2);
        }

        public double Div(double num1, double num2)
        {
            return math.Div(num1, num2);
        }

        #endregion
    }
}
