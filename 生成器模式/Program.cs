﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 生成器模式
{
    class Program
    {
        private static IBuilder appleBuilder = new ApplePhoneBuilder();
        private static IBuilder samBuilder = new SamsungPhoneBuilder();
        static void Main(string[] args)
        {
            Director director = new Director(appleBuilder);
            director.Construct();
            Phone phone = appleBuilder.GetPhone();
            Console.WriteLine("IPhone");
            phone.Show();

            director = new Director(samBuilder);
            director.Construct();
            phone = samBuilder.GetPhone();
            Console.WriteLine("SamSung");
            phone.Show();

            Console.ReadLine();
        }
    }
}
