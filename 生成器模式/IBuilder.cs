﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 生成器模式
{
    public interface IBuilder
    {
        void BuildCPU();
        void BuildScreen();
        void BuildBattery();
        Phone GetPhone();
    }
}
