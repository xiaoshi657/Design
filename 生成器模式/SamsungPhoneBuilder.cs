﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 生成器模式
{
    public class SamsungPhoneBuilder:IBuilder
    {
        private Phone phone = new SamsungPhone();

        public void BuildCPU()
        {
            phone.Add("CPU:MTK");
        }

        public void BuildScreen()
        {
            phone.Add("Screen:Samsung");
        }

        public void BuildBattery()
        {
            phone.Add("Battery:Desai");
        }

        public Phone GetPhone()
        {
            return phone;
        }
    }
}
