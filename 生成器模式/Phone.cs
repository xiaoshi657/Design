﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 生成器模式
{
    public abstract class Phone
    {
        protected List<string> parts = new List<string>();

        public void Add(string part)
        {
            parts.Add(part);
        }

        public void Show()
        {
            Console.WriteLine("产品部件信息：");
            foreach (var part in parts)
            {
                Console.WriteLine(part+"\t");
            }
        }
    }
}
