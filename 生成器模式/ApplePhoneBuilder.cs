﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 生成器模式
{
    public class ApplePhoneBuilder:IBuilder
    {
        private Phone phone = new ApplePhone();

        public void BuildCPU()
        {
            phone.Add("CPU:Qualcomm");
        }

        public void BuildScreen()
        {
            phone.Add("Screen:JDI");
        }

        public void BuildBattery()
        {
            phone.Add("Battery:Desai");
        }

        public Phone GetPhone()
        {
            return phone;
        }
    }
}
