﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 策略模式
{
    public abstract class Flyable
    {
        public abstract void Flying();
    }
}
