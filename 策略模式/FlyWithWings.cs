﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 策略模式
{
    public class FlyWithWings:Flyable
    {
        public override void Flying()
        {
            Console.WriteLine("Flying...");
        }
    }
}
