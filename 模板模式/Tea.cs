﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 模板模式
{
    public class Tea:CaffeineBeverage
    {
        void prepareRecipe()
        { 
            
        }

        public override void brew()
        {
            Console.WriteLine("Dripping tea through filter");
        }

        public override void addCondiments()
        {
            Console.WriteLine("Adding Sugar and Milk");
        }
    }
}
