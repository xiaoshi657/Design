﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 模板模式
{
    public abstract class CaffeineBeverage
    {
        void prepareRecipe()
        {
            boilWater();

            brew();

            pourInCup();

            addCondiments();
        }

        public abstract void brew();

        public abstract void addCondiments();

        void boilWater()
        {
            Console.WriteLine("Boiling water");
        }

        void pourInCup()
        {
            Console.WriteLine("Pouring into cup");
        }
    }
}
