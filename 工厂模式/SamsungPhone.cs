﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 工厂模式
{
    public class SamsungPhone:IPhone
    {
        public void Call(string phoneNo)
        {
            Console.WriteLine("This is call from Samsung phone to " + phoneNo + ".");
        }

        public void Sms(string phoneNo, string smsContent)
        {
            Console.WriteLine("This message is send from Samsung phone to " + phoneNo + ", the content is " + smsContent + ".");
        }
    }
}
