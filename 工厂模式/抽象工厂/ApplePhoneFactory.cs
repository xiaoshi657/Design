﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 工厂模式
{
    public class ApplePhoneFactory:IProvider
    {
        public IPhone Produce()
        {
            return new ApplePhone();
        }
    }
}
