﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;

namespace 工厂模式.抽象工厂.SqlHelper
{
    public class MSSqlFactory:IDBFactory
    {
        #region 创建数据库连接对象
        /// <summary>
        /// 创建数据库连接对象
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <returns></returns>
        public DbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }
        #endregion

        #region 创建数据库参数对象
        /// <summary>
        /// 创建数据库参数对象
        /// </summary>
        /// <param name="name">参数名</param>
        /// <param name="value">参数值</param>
        /// <returns></returns>
        public DbParameter CreateParameter(string name, object value)
        {
            return new SqlParameter(name, value);
        }
        #endregion

        #region 创建数据库命令对象
        /// <summary>
        /// 创建数据库命令对象
        /// </summary>
        /// <param name="strSql">数据库连接字符串</param>
        /// <param name="strSql">sql语句</param>
        /// <param name="parms">sql参数</param>
        /// <returns></returns>
        public DbCommand CreateCommand(DbConnection conn, string strSql, params DbParameter[] parms)
        {
            SqlConnection sqlConn = conn as SqlConnection;
            SqlCommand cmd= sqlConn.CreateCommand();
            cmd.CommandText = strSql;
            cmd.Parameters.AddRange(parms);
            return cmd;
        }
        #endregion

        #region 创建数据库命令对象
        /// <summary>
        /// 创建数据库命令对象
        /// </summary>
        /// <param name="strSql">sql语句</param>
        /// <param name="parms">sql参数</param>
        /// <returns></returns>
        public DbCommand CreateCommand(string strSql, params DbParameter[] parms)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = strSql;
            cmd.Parameters.AddRange(parms);
            return cmd;
        }
        #endregion

        #region 执行非查询操作
        /// <summary>
        /// 执行非查询操作
        /// </summary>
        /// <param name="cmd">数据库命令对象</param>
        /// <param name="strSql">sql语句</param>
        /// <param name="parms">sql参数</param>
        /// <returns></returns>
        public int ExecuteNoQueryAsync(DbCommand cmd,string strSql, params DbParameter[] parms)
        {
            try
            {
                int result = 0;
                SqlCommand sqlCmd = cmd as SqlCommand;
                sqlCmd.CommandText = strSql;
                sqlCmd.Parameters.AddRange(parms);
                sqlCmd.BeginExecuteNonQuery(ar => {
                    result = sqlCmd.EndExecuteNonQuery(ar);
                }, null);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion

        public int ExecuteNoQuery(string strSql, params DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public object ExecuteScalarAsync(string strSql, params DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public object ExecuteScalar(string strSql, params DbParameter[] parms)
        {
            throw new NotImplementedException();
        }
    }
}
