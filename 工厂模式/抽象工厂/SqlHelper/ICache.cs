﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace 工厂模式.抽象工厂.SqlHelper
{
    public interface ICache<T>
    {
        /// <summary>
        /// 缓存参数对象
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="name">参数名</param>
        /// <param name="parm">参数对象</param>
        void Insert(string connectionString, string name, T parm);
        /// <summary>
        /// 移除参数对象
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="name">参数名</param>
        void Remove(string connectionString, string name);
        /// <summary>
        /// 获得参数对象
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="name">参数名</param>
        /// <returns></returns>
        T GetParameter(string connectionString, string name);
    }
}
