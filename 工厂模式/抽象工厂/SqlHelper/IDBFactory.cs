﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace 工厂模式.抽象工厂.SqlHelper
{
    public interface IDBFactory
    {
        /// <summary>
        /// 创建数据库连接对象
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <returns></returns>
        DbConnection CreateConnection(string connectionString);
        /// <summary>
        /// 创建数据库参数对象
        /// </summary>
        /// <param name="name">参数名</param>
        /// <param name="value">参数值</param>
        /// <returns></returns>
        DbParameter CreateParameter(string name, object value);
        /// <summary>
        /// 创建数据库命令对象
        /// </summary>
        /// <param name="strSql">数据库连接字符串</param>
        /// <param name="strSql">sql语句</param>
        /// <param name="parms">sql参数</param>
        /// <returns></returns>
        DbCommand CreateCommand(DbConnection conn, string strSql, params DbParameter[] parms);
        /// <summary>
        /// 创建数据库命令对象
        /// </summary>
        /// <param name="strSql">sql语句</param>
        /// <param name="parms">sql参数</param>
        /// <returns></returns>
        DbCommand CreateCommand(string strSql, params DbParameter[] parms);
        /// <summary>
        /// 执行非查询操作
        /// </summary>
        /// <param name="cmd">数据库命令对象</param>
        /// <param name="strSql">sql语句</param>
        /// <param name="parms">sql参数</param>
        /// <returns></returns>
        int ExecuteNoQueryAsync(DbCommand cmd, string strSql, params DbParameter[] parms);
        /// <summary>
        /// 执行非查询操作
        /// </summary>
        /// <param name="strSql">sql语句</param>
        /// <param name="parms">sql参数</param>
        /// <returns></returns>
        int ExecuteNoQuery(string strSql, params DbParameter[] parms);
        /// <summary>
        /// 执行单个查询
        /// </summary>
        /// <param name="strSql">sql语句</param>
        /// <param name="parms">sql参数</param>
        /// <returns></returns>
        object ExecuteScalarAsync(string strSql, params DbParameter[] parms);
        /// <summary>
        /// 执行单个查询
        /// </summary>
        /// <param name="strSql">sql语句</param>
        /// <param name="parms">sql参数</param>
        /// <returns></returns>
        object ExecuteScalar(string strSql, params DbParameter[] parms);
    }
}
