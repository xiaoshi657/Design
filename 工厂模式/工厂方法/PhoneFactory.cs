﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 工厂模式
{
    public abstract class PhoneFactory
    {
        /// <summary>
        /// 工厂方法模式
        /// </summary>
        /// <param name="phoneType"></param>
        /// <returns></returns>
        public IPhone FactoryMethod(PhoneType phoneType)
        {
            IPhone phone = Produce(phoneType);
            return phone;
        }

        public abstract IPhone Produce(PhoneType phoneType);
    }
}
