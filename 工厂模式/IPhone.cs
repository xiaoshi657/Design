﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 工厂模式
{
    public interface IPhone
    {
        void Call(string phoneNo);
        void Sms(string phoneNo, string smsContent);
    }
}
