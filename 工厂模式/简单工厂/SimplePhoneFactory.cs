﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 工厂模式
{
    public class SimplePhoneFactory
    {
        /// <summary>
        /// 简单工厂模式
        /// </summary>
        /// <param name="phoneType"></param>
        /// <returns></returns>
        public static IPhone Produce(PhoneType phoneType)
        {
            if (phoneType == PhoneType.Apple)
            {
                return new ApplePhone();
            }
            else if (phoneType == PhoneType.Samsung)
            {
                return new SamsungPhone();
            }
            else
            {
                return null;
            }
        }
    }
}
